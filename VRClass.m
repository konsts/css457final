classdef VRClass
   methods(Static)
       
       %This function breaks down the input file into the block frames
       function M3 = blockFrames(s, fs, distanceBtwBeginning, samplesPerFrame)
            l = length(s);
            numberOfFrames = floor((l - samplesPerFrame) / distanceBtwBeginning) + 1;
            for i = 1:samplesPerFrame
                for j = 1:numberOfFrames
                    M(i, j) = s(((j - 1) * distanceBtwBeginning) + i); 
                end
            end
            h = hamming(samplesPerFrame);
            M2 = diag(h) * M;
            for i = 1:numberOfFrames
                M3(:, i) = fft(M2(:, i));
            end
       end
       
       %This function finds the filter bank amplitudes
       function m = melfb(p, n, fs)
            f0 = 700 / fs; 
            fn2 = floor(n/2); 
            lr = log(1 + 0.5/f0) / (p+1);  
            bl = n * (f0 * (exp([0 1 p p+1] * lr) - 1)); 
            b1 = floor(bl(1)) + 1; 
            b2 = ceil(bl(2)); 
            b3 = floor(bl(3)); 
            b4 = min(fn2, ceil(bl(4))) - 1; 
            pf = log(1 + (b1:b4)/n/f0) / lr; 
            fp = floor(pf); 
            pm = pf - fp; 
            r = [fp(b2:b4) 1+fp(1:b3)]; 
            c = [b2:b4 1:b3] + 1; 
            v = 2 * [1-pm(b2:b4) pm(1:b3)]; 
            m = sparse(r, c, v, p, 1+fn2); 
       end
       
       %This function computes the MFCC coefficients
       function r = mfcc(s, fs)
            m = 100;    %distance between the beginning of each fram
            n = 256;    %number of samples per frame
            frame = VRClass.blockFrames(s, fs, m, n);
            m = VRClass.melfb(20, n, fs);
            n2 = 1 + floor(n / 2);
            z = m * abs(frame(1:n2, :)).^2;
            r = dct(log(z));
       end
       
       %This function assembles the codebook by vector quantization
       function r = vqlbg(d,k)
            e = .01;
            r = mean(d, 2);
            dpr = 10000;
            for i = 1:log2(k)
                r = [r*(1+e), r*(1-e)];
                while (1 == 1)
                    z = disteu(d, r);
                    [m,ind] = min(z, [], 2);
                    t = 0;
                    for j = 1:2^i
                        r(:, j) = mean(d(:, find(ind == j)), 2); 
                        x = disteu(d(:, find(ind == j)), r(:, j)); 
                        for q = 1:length(x)
                            t = t + x(q);
                        end
                    end
                    if (((dpr - t)/t) < e)
                        break;
                    else
                        dpr = t;
                    end
                end
            end
       end
       
       %This function finds the Eucledian distances
       function d = disteu(x, y)
            [M, N] = size(x);
            [M2, P] = size(y);
            if (M ~= M2)
                error('Matrix dimensions do not match.')
            end
            d = zeros(N, P);
            for ii=1:N
                for jj=1:P
                    d(ii,jj) = VRClass.mydistance(x(:,ii),y(:,jj),2);
                end
            end
       end

       %This function finds the weighted distances 
       function [out] = mydistance(x,y,tipo)
            if tipo == 0
                out = sum((x-y).^2).^0.5;
            end
            
            if tipo == 1
                out = sum(abs(x-y));
            end
            % Weighted distance
            if tipo == 2
                pesi = zeros(size(x));
                pesi(1) = 0.20;
                pesi(2) = 0.90;
                pesi(3) = 0.95;
                pesi(4) = 0.90;
                pesi(5) = 0.70;
                pesi(6) = 0.90;
                pesi(7) = 1.00;
                pesi(8) = 1.00;
                pesi(9) = 1.00;
                pesi(10) = 0.95;
                pesi(11:13) = 0.30;
                out = sum(abs(x-y).*pesi);
            end
       end
       
       
       function computeAndPrint(inputFileName, arrOfLibFiles, arrOfLibLabels, threshold, vqpoints)
        
            minDist = Inf;
            distIndex = 0;
            
            %getting the data from the input file
            [newFileData, Fs] = audioread(inputFileName);
            %getting the mfcc from the input file
            mfccN = VRClass.mfcc(newFileData,Fs);
            %getting the vector quatization coefficients from the input
            %file
            vqN = VRClass.vqlbg(mfccN,vqpoints);
            
            %length of the library array
            [rows, length] = size(arrOfLibFiles);
            
            for i = 1:length
                [audioData{i}, Fs] = audioread(arrOfLibFiles{i}); 
                mfccData{i} = VRClass.mfcc(audioData{i},Fs);
                vqData{i} = VRClass.vqlbg(mfccData{i},vqpoints);
                distM{i} = VRClass.disteu(mfccData{i}, vqN);
                distances{i} = sum(min(distM{i},[],2)) / size(distM{i},1);
                str = fprintf('Distance to %s  \t', arrOfLibLabels{i});
                disp(num2str(distances{i}));
                if distances{i} < minDist
                    minDist = distances{i};
                    distIndex = i;
                end
            end
            
            if minDist > threshold
                disp('No match found');
            else
                str = sprintf('The closest sound from the library is : %s', arrOfLibLabels{distIndex});
                disp(str);
            end

       end
       
   end    
end    