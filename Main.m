clear all;
fileNames = {'on.wav', 'off.wav', 'high.wav', 'low.wav'};
labels = {'on', 'off', 'high', 'low'};
threshold = 8;
vqpoints = 16;

VRClass.computeAndPrint('on.wav', fileNames, labels, threshold, vqpoints);
 
